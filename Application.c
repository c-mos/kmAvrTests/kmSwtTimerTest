/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//*
* Application.c
*
*  Created on: Oct 20, 2022
*      Author: Krzysztof Moskwa
*      License: GPL-3.0-or-later
*
*  Test Application for kmAvrLibs
*  Copyright (C) 2022  Krzysztof Moskwa
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdlib.h>
#include <stdio.h>

#include "kmCpu/kmCpu.h"
#include "kmDebug/kmDebug.h"
#include "kmSwtTimer/kmSoftwareTimer.h"

// "private" variables
static const uint16_t _defaultTimeout = 500; // milliseconds

// "private" functions
void _callbackDebugLed(const void *userData, kmSwtIntervalValueType *newTimerValue)  {
	dbToggle(DB_PIN_0);
	uint16_t myTimeout = (uint16_t)userData;
	*newTimerValue = myTimeout;
}

// "public" functions
void appInit(void) {
	dbPullUpAllPorts();
	dbInit();
	dbOff(DB_PIN_0);

	kmSwtInit(KM_SWT_INTERVAL_10MS);
	kmSwtRegisterCallback(KM_SWT_TIMER_0, KM_SWT_USER_DATA(_defaultTimeout), _callbackDebugLed);
	kmSwtStart(KM_SWT_TIMER_0, _defaultTimeout);

	kmCpuInterruptsEnable();
}

void appLoop(void) {
	kmSwtLoop();
}
