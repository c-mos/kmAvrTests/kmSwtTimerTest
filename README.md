# kmSwtTimerTest Application

## Overview

This application, `kmSwtTimerTest`, is designed to test the [`kmSwtTimer`](https://gitlab.com/c-mos/kmAvrLibs/kmSwtTimer) library from the AVR `kmFramework`. It serves also as a demonstration of various functionalities provided by the library.

## Getting sources and compilation
To get the sources, make sure your [gitlab.com](https://gitlab.com) account is created and correctly configured (e.g., public key defined), then use the following git commands to get the source code:
```bash
git clone https://gitlab.com/c-mos/kmAvrTests/kmSwtTimerTest
cd kmSwtTimerTest
git submodule update --init
git submodule foreach git checkout main
```
The software has been compiled and tested with [**Atmel Studio 7**](http://atmel-studio.s3-website-us-west-2.amazonaws.com/7.0.2397/as-installer-7.0.2397-full.exe).

## Features
- **Debug LED Toggle**: The application initializes a debug LED, which toggles its state based on a software timer callback. This demonstrates how to use a software timer to periodically execute a task.
- **Software Timer Initialization**: Initializes the software timer with a 10ms interval. This sets up the basic timing mechanism for the application.
- **Callback Registration**: Registers a callback function (`_callbackDebugLed`) to be called when the software timer reaches the specified timeout. This function toggles the debug LED and resets the timer value.
- **User Data Handling**: Passes user data (timeout value) to the callback function, illustrating how to use custom data with software timer events.
- **CPU Interrupts**: Enables CPU interrupts, which are necessary for the software timer and other interrupt-driven functionalities to operate correctly.
- **Main Loop Processing**: The `appLoop()` function continuously processes the software timer events, ensuring that the timer callbacks are called at the appropriate intervals.

## Dependencies

This application depends on the following libraries:
- [kmFrameworkAVR](https://gitlab.com/c-mos/kmAvrLibs)
  - [kmCommon](https://gitlab.com/c-mos/kmAvrLibs/kmCommon)
  - [kmCpu](https://gitlab.com/c-mos/kmAvrLibs/kmCpu)
  - [kmDebug](https://gitlab.com/c-mos/kmAvrLibs/kmDebug)
  - [kmTimersCommon](https://gitlab.com/c-mos/kmAvrLibs/kmTimersCommon)
  - [kmSwtTimer](https://gitlab.com/c-mos/kmAvrLibs/kmSwtTimer)

Ensure that these libraries are properly included and configured in your project.

## Author and License
Author: Krzysztof Moskwa

e-mail: chris[dot]moskva[at]gmail[dot]com

Software License: GNU General Public License (GPL) version 3.0 or later. See [LICENSE.txt](https://www.gnu.org/licenses/gpl-3.0.txt).

![GPL3 Logo](https://www.gnu.org/graphics/gplv3-or-later-sm.png)

