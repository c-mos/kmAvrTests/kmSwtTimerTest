/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//** @file
* @brief Version file for kmAvrTest.
* config.h
*
*  **Created on**: Oct 20, 2022 @n
*      **Author**: Krzysztof Moskwa @n
*      **License**: GPL-3.0-or-later @n@n
*
*  Test Application for kmAvrLibs @n
*  **Copyright (C) 2019  Krzysztof Moskwa**
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/

#ifndef CONFIG_H_
#define CONFIG_H_

/*
To use own configuration for this module
- uncomment line below (#define KM_DEBUG_USE_DEFAULT_CONFIG), so default config is not included
- copy content of "kmDebug/kmDebugDefaultConfig.h" file here
- adjust default settings to your needs
*/
#define KM_DEBUG_USE_DEFAULT_CONFIG

// kmDebug
#ifdef KM_DEBUG_USE_DEFAULT_CONFIG
#include "kmDebug/kmDebugDefaultConfig.h"
#else
/// Definition of the Direction Register for port used in debugging
#define KM_DEBUG_DDR DDRB
/// Definition of the Port Register for debugging
#define KM_DEBUG_PORT PORTB
/// Reference of physical pin to KM_DEBUG_PIN_0 
#define KM_DEBUG_PIN_0 PB4
#endif

// kmSwtTimer
#include "kmSwtTimer/kmSoftwareTimerDefaultConfig.h"

#endif /* CONFIG_H_ */
